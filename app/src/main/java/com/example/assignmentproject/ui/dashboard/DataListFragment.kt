package com.example.assignmentproject.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.assignmentproject.R

class DataListFragment : Fragment() {

    private lateinit var dashboardViewModel: DataListViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dashboardViewModel =
                ViewModelProviders.of(this).get(DataListViewModel::class.java)

        val root = inflater.inflate(R.layout.data_list, container, false)

        return root
    }
}
