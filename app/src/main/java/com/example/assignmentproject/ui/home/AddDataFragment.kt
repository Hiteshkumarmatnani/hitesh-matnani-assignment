package com.example.assignmentproject.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.assignmentproject.R

class AddDataFragment : Fragment() {

    private lateinit var homeViewModel: AddDataViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        homeViewModel =
                ViewModelProviders.of(this).get(AddDataViewModel::class.java)

        val root = inflater.inflate(R.layout.add_data, container, false)

        return root
    }
}
