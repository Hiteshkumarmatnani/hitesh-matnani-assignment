package com.example.assignmentproject.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.assignmentproject.R

class SearchDataFragment : Fragment() {

    private lateinit var notificationsViewModel: SearchDataViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
                ViewModelProviders.of(this).get(SearchDataViewModel::class.java)

        val root = inflater.inflate(R.layout.search_data, container, false)

        return root
    }
}
